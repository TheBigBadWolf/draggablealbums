import { useQuery } from 'react-query';

import { AlbumT } from '../models';

const fetchAlbums = async (): Promise<AlbumT[]> => {
  const albums = await fetch(`${process.env.REACT_APP_SERVER_API}/albums`);
  if (!albums.ok) throw new Error('failed to fetch albums');
  return albums.json();
};

export const useGetAlbums = () => {
  const {
    isLoading: isLoadingAlbums,
    isError: isErrorAlbums,
    data: albums,
  } = useQuery<AlbumT[], Error>('album', () => fetchAlbums());

  return { isLoadingAlbums, isErrorAlbums, albums: albums || [] };
};
