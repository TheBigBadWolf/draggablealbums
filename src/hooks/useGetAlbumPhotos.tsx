import { useQuery } from 'react-query';

import { PhotoT } from '../models';

const fetchAlbumPhotos = async (albumId: number): Promise<PhotoT[]> => {
  const albumPhotos = await fetch(
    `${process.env.REACT_APP_SERVER_API}/albums/${albumId}/photos`
  );
  if (!albumPhotos.ok) throw new Error('failed to fetch album photos');
  return albumPhotos.json();
};

export const useGetAlbumPhotos = (
  isEnabled: boolean = true,
  albumId: number,
  onSuccess: (photos: PhotoT[]) => void
) => {
  const {
    isLoading: isLoadingAlbumPhotos,
    isError: isErrorAlbumPhotos,
    data: albumPhotos,
    refetch: refetchAlbumPhotos,
  } = useQuery<PhotoT[], Error>(['album-photos', albumId], () => fetchAlbumPhotos(albumId), {
    enabled: isEnabled,
    onSuccess,
  });

  return {
    isLoadingAlbumPhotos,
    isErrorAlbumPhotos,
    albumPhotos: albumPhotos || [],
    refetchAlbumPhotos,
  };
};
