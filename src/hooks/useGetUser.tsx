import { useQuery } from 'react-query';

import { UserT } from '../models';

const fetchUser = async (userId: number): Promise<UserT> => {
  const user = await fetch(`${process.env.REACT_APP_SERVER_API}/users/${userId}`);
  if (!user.ok) throw new Error('failed to fetch users');
  return user.json();
};

export const useGetUser = (userId: number) => {
  const {
    isLoading: isLoadingUser,
    isError: isErrorUser,
    data: user,
  } = useQuery<UserT, Error>(['user', userId], () => fetchUser(userId));

  return { isLoadingUser, isErrorUser, user };
};
