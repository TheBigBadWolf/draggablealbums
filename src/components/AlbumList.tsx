import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import WithLoadingSpinner from './WithLoadingSpinner';

import Album from './Album';
import { useGetAlbums } from '../hooks';

const AlbumList = () => {
  const { albums, isErrorAlbums, isLoadingAlbums } = useGetAlbums();

  if (isErrorAlbums)
    return (
      <Typography variant="subtitle1">
        Sorry, an error occurrred while displaying the albums info :(
      </Typography>
    );
  if (isLoadingAlbums)
    return (
      <WithLoadingSpinner>
        <Typography variant="subtitle1">Preparing the albums for you</Typography>
      </WithLoadingSpinner>
    );

  return (
    <Box>
      {albums.map(({ id: albumId, userId, title }) => (
        <Album key={albumId} id={albumId} userId={userId} title={title} />
      ))}
    </Box>
  );
};

export default AlbumList;
