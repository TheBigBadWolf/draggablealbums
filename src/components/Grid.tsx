import React, { ReactNode } from 'react';
import Box from '@mui/material/Box';

const Grid = ({ children, columns }: { children: ReactNode; columns: number }) => {
  return (
    <Box
      sx={{
        display: 'grid',
        gridTemplateColumns: `repeat(${columns}, 1fr)`,
        gap: 1.5,
      }}
    >
      {children}
    </Box>
  );
};

export default Grid;
