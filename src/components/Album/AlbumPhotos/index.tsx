import React, { useState } from 'react';
import {
  DndContext,
  closestCenter,
  MouseSensor,
  TouchSensor,
  DragOverlay,
  useSensor,
  useSensors,
  DragStartEvent,
  DragEndEvent,
} from '@dnd-kit/core';
import { arrayMove, SortableContext, rectSortingStrategy } from '@dnd-kit/sortable';
import Typography from '@mui/material/Typography';

import Grid from '../../Grid';
import SortablePhoto from './SortablePhoto';
import Photo from './Photo';
import WithLoadingSpinner from '../../WithLoadingSpinner';
import { PhotoT } from '../../../models';

const AlbumPhotos = ({
  photos,
  onChangePhotos,
  onDeletePhoto,
  onClickPhoto,
  isErrorAlbumPhotos,
  isLoadingAlbumPhotos,
}: {
  photos: PhotoT[];
  onChangePhotos: React.Dispatch<React.SetStateAction<PhotoT[]>>;
  onDeletePhoto: (photoId: PhotoT['id']) => void;
  onClickPhoto: (photoId: PhotoT['id']) => void;
  isErrorAlbumPhotos: boolean;
  isLoadingAlbumPhotos: boolean;
}) => {
  const [activePhotoId, setActivePhotoId] = useState<PhotoT['id'] | null>(null);
  const sensors = useSensors(useSensor(MouseSensor), useSensor(TouchSensor));

  const handleDragStart = (event: DragStartEvent) => {
    setActivePhotoId(+event.active.id);
  };

  const handleDragEnd = (event: DragEndEvent) => {
    const { active, over } = event;
    if (over?.id && active.id !== over.id) {
      onChangePhotos((photos) => {
        const oldIndex = photos.findIndex((photo) => photo.id === active.id);
        const newIndex = photos.findIndex((photo) => photo.id === over.id);
        return arrayMove(photos, oldIndex, newIndex);
      });
    }

    setActivePhotoId(null);
  };

  const handleDragCancel = () => {
    setActivePhotoId(null);
  };

  const draggedPhoto = photos.find((photo) => photo.id === activePhotoId);

  if (isErrorAlbumPhotos) {
    return (
      <Typography variant="subtitle1">
        Sorry, An error occurrred while displaying the album's photos :(
      </Typography>
    );
  }

  if (isLoadingAlbumPhotos) {
    return (
      <WithLoadingSpinner>
        <Typography variant="subtitle1">Preparing the photos for you</Typography>
      </WithLoadingSpinner>
    );
  }

  return (
    <DndContext
      sensors={sensors}
      collisionDetection={closestCenter}
      onDragStart={handleDragStart}
      onDragEnd={handleDragEnd}
      onDragCancel={handleDragCancel}
    >
      <SortableContext items={photos} strategy={rectSortingStrategy}>
        <Grid columns={4}>
          {photos.map((photo) => (
            <SortablePhoto
              key={photo.id}
              photo={photo}
              onDeletePhoto={onDeletePhoto}
              onClickPhoto={onClickPhoto}
            />
          ))}
        </Grid>
      </SortableContext>
      <DragOverlay adjustScale>
        {draggedPhoto && (
          <Photo
            onDeletePhoto={(_: PhotoT['id']) => {}}
            onClickPhoto={(_: PhotoT['id']) => {}}
            photo={draggedPhoto}
            isDragging
          />
        )}
      </DragOverlay>
    </DndContext>
  );
};

export default AlbumPhotos;
