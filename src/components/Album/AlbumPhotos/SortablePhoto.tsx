import React, { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';

import Photo, { PhotoPropsT } from './Photo';

const SortablePhoto = (props: PhotoPropsT) => {
  const { isDragging, attributes, listeners, setNodeRef, transform, transition } = useSortable(
    { id: props.photo.id }
  );

  const style = {
    transform: CSS.Transform.toString(transform),
    transition: transition || undefined,
  };

  return (
    <Photo
      ref={setNodeRef}
      style={style}
      withOpacity={isDragging}
      {...props}
      {...attributes}
      {...listeners}
    />
  );
};

export default SortablePhoto;
