import React, { forwardRef, HTMLAttributes, CSSProperties, useState } from 'react';
import Tooltip from '@mui/material/Tooltip';

import WithCloseIcon from '../../WithCloseIcon';
import { PhotoT } from '../../../models';

export type PhotoPropsT = HTMLAttributes<HTMLImageElement> & {
  photo: PhotoT;
  withOpacity?: boolean;
  isDragging?: boolean;
  onDeletePhoto: (photoId: PhotoT['id']) => void;
  onClickPhoto: (photoId: PhotoT['id']) => void;
};

const Photo = forwardRef<HTMLImageElement, PhotoPropsT>(
  ({ withOpacity, isDragging, style, photo, onDeletePhoto, onClickPhoto, ...props }, ref) => {
    const { onMouseDown, ...sortableProps } = props;

    const [isPhotoHovered, setIsPhotoHovered] = useState(false);

    const photoContainerStyles: CSSProperties = {
      height: 150,
      width: '100%',
      opacity: withOpacity ? 0.5 : 1,
      cursor: isDragging ? 'grabbing' : 'grab',
      transform: isDragging ? 'scale(1.05)' : 'scale(1)',
      ...style,
    };

    return (
      <WithCloseIcon
        onClickCloseIcon={() => onDeletePhoto(photo.id)}
        style={{ ...photoContainerStyles }}
      >
        <Tooltip title={photo.title} placement="top" open={!isDragging && isPhotoHovered}>
          <img
            ref={ref}
            onMouseEnter={() => {
              setIsPhotoHovered(true);
            }}
            onMouseLeave={() => {
              setIsPhotoHovered(false);
            }}
            width="100%"
            height="100%"
            src={photo.thumbnailUrl}
            alt={photo.title}
            loading="lazy"
            onDragStart={onMouseDown}
            onClick={() => {
              onClickPhoto(photo.id);
            }}
            style={{ borderRadius: 4 }}
            {...sortableProps}
          />
        </Tooltip>
      </WithCloseIcon>
    );
  }
);

export default Photo;
