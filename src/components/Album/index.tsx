import React, { useState, SyntheticEvent } from 'react';
import { makeStyles } from 'tss-react/mui';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import AlbumInfo from './AlbumInfo';
import AlbumPhotos from './AlbumPhotos';
import PopupPhoto from './PopupPhoto';
import { useGetUser, useGetAlbumPhotos } from '../../hooks';
import { AlbumT, PhotoT } from '../../models';

const useStyles = makeStyles()({
  accordionContent: {
    flexDirection: 'column',
    alignItems: 'center',
  },
});

const Album = ({ id: albumId, userId, title }: AlbumT) => {
  const { classes } = useStyles();
  const { isLoadingUser, isErrorUser, user } = useGetUser(userId);

  const [isExpanded, setIsExpanded] = useState(false);
  const [popupPhotoId, setPopupPhotoId] = useState<PhotoT['id'] | null>(null);
  const [photos, setPhotos] = useState<PhotoT[]>([]);

  const { isLoadingAlbumPhotos, isErrorAlbumPhotos, refetchAlbumPhotos } = useGetAlbumPhotos(
    isExpanded,
    albumId,
    (albumPhotos: PhotoT[]) => {
      setPhotos(albumPhotos.slice(0, 12));
    }
  );

  const handleDeletePhoto = (id: PhotoT['id']) => {
    const updatedEditedAlbumPhotos = photos.filter((albumPhoto) => albumPhoto.id !== id);
    setPhotos(updatedEditedAlbumPhotos);
  };

  const handleToggleAccordion = async (e: SyntheticEvent, isExpanded: boolean) => {
    setIsExpanded(isExpanded);
    if (isExpanded) {
      await refetchAlbumPhotos();
    }
  };

  const popupPhoto = photos.find((photo) => photo.id === popupPhotoId);

  if (isLoadingUser) return null;

  return (
    <>
      {popupPhoto && (
        <PopupPhoto photo={popupPhoto} onClosePhoto={() => setPopupPhotoId(null)} />
      )}
      <Accordion expanded={isExpanded} onChange={handleToggleAccordion}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls={`panel-${albumId}-content`}
          id={`panel-${albumId}-header`}
          sx={{ marginLeft: 4 }}
          classes={{ content: classes.accordionContent }}
        >
          <AlbumInfo
            albumId={albumId}
            albumTitle={title}
            userName={user?.name}
            userEmail={user?.email}
            isErrorUser={isErrorUser}
          />
        </AccordionSummary>
        <AccordionDetails sx={{ display: 'flex', justifyContent: 'center' }}>
          <AlbumPhotos
            photos={photos}
            onDeletePhoto={handleDeletePhoto}
            onChangePhotos={setPhotos}
            onClickPhoto={(photoId: PhotoT['id']) => setPopupPhotoId(photoId)}
            isErrorAlbumPhotos={isErrorAlbumPhotos}
            isLoadingAlbumPhotos={isLoadingAlbumPhotos}
          />
        </AccordionDetails>
      </Accordion>
    </>
  );
};

export default Album;
