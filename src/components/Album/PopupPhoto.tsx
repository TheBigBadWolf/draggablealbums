import React, { makeStyles } from 'tss-react/mui';
import Dialog from '@mui/material/Dialog';

import WithCloseIcon from '../WithCloseIcon';
import { PhotoT } from '../../models';

const useStyles = makeStyles()({
  dialogPaper: {
    height: '80%',
  },
});

const PopupPhoto = ({ photo, onClosePhoto }: { photo: PhotoT; onClosePhoto: () => void }) => {
  const { classes } = useStyles();
  return (
    <Dialog open onClose={onClosePhoto} classes={{ paper: classes.dialogPaper }}>
      <WithCloseIcon onClickCloseIcon={onClosePhoto} style={{ lineHeight: 0 }}>
        <img width="100%" height="100%" src={photo.thumbnailUrl} alt={photo.title} />
      </WithCloseIcon>
    </Dialog>
  );
};

export default PopupPhoto;
