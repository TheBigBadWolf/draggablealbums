import React, { useTheme } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Divider from '@mui/material/Divider';
import useMediaQuery from '@mui/material/useMediaQuery';
import PersonIcon from '@mui/icons-material/Person';
import EmailIcon from '@mui/icons-material/Email';

import WithSubtitle from '../WithSubtitle';

const AlbumInfo = ({
  albumId,
  albumTitle,
  userName,
  userEmail,
  isErrorUser,
}: {
  albumId: number;
  albumTitle: string;
  userName?: string;
  userEmail?: string;
  isErrorUser: boolean;
}) => {
  const theme = useTheme();
  const matchesMd = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Grid container direction="column">
      <Grid item marginBottom={2}>
        <Typography variant="h2" align={matchesMd ? 'center' : undefined}>
          {albumId}. {albumTitle}
        </Typography>
      </Grid>
      {isErrorUser ? (
        <Grid item>
          <Typography variant="subtitle1">
            Sorry, An error occurrred while displaying the user info :(
          </Typography>
        </Grid>
      ) : (
        <Grid item container gap={4} justifyContent={matchesMd ? 'center' : undefined}>
          <Grid item>
            <WithSubtitle subtitle={userName!}>
              <PersonIcon />
            </WithSubtitle>
          </Grid>
          {!matchesMd && <Divider orientation="vertical" variant="middle" flexItem />}
          <Grid item>
            <WithSubtitle subtitle={userEmail!}>
              <EmailIcon />
            </WithSubtitle>
          </Grid>
        </Grid>
      )}
    </Grid>
  );
};

export default AlbumInfo;
