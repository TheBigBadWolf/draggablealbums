import React, { ReactNode } from 'react';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';

const WithSubtitle = ({ subtitle, children }: { subtitle: string; children: ReactNode }) => {
  return (
    <Grid item container>
      <Grid item>{children}</Grid>
      <Grid item>
        <Typography variant="subtitle1">{subtitle}</Typography>
      </Grid>
    </Grid>
  );
};

export default WithSubtitle;
