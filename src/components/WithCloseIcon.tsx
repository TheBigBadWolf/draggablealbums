import React, { ReactNode } from 'react';
import Box from '@mui/material/Box';
import CloseIcon from '@mui/icons-material/Close';

const WithCloseIcon = ({
  onClickCloseIcon,
  children,
  style,
}: {
  onClickCloseIcon: () => void;
  children: ReactNode;
  style: React.CSSProperties;
}) => {
  return (
    <Box sx={{ position: 'relative', height: '100%', ...style }}>
      <CloseIcon
        sx={{
          position: 'absolute',
          top: 5,
          right: 5,
          cursor: 'pointer',
        }}
        onClick={onClickCloseIcon}
      />
      {children}
    </Box>
  );
};

export default WithCloseIcon;
